$(function() {
    $('.nav-toggle').on('click', function() {
        if ( $(this).hasClass('active') ) {
            close_navigation_collapse( $('.nav') )
        } else {
            open_navigation_collapse( $('.nav') )
        }
    });
    $(document).on('click', function(e) {
        if ( !$(e.target).closest('.nav').length && $('.nav-collapse.open').length > 0 ) {
            close_navigation_collapse( $('.nav') )
        }
    });

    function close_navigation_collapse(nav) {
        let collapse = nav.find('.nav-collapse');
        collapse.clearQueue();
        let duration = collapse.data('collapse-duration');
        if (!duration) {
            duration = 250;
        }
        let toggle = nav.find('.nav-toggle');
        toggle.removeClass('active');
        collapse.animate({
            'height': 0
        }, duration, function() {
            collapse.removeClass('open').css('height', '');
        });
    }

    function open_navigation_collapse(nav) {
        let collapse = nav.find('.nav-collapse');
        collapse.clearQueue();
        let duration = collapse.data('collapse-duration');
        if (!duration) {
            duration = 250;
        }
        let toggle = nav.find('.nav-toggle');
        toggle.addClass('active');
        collapse.css('height', 'auto');
        let cHeight = collapse.outerHeight();
        collapse.css('height', 0);
        collapse.animate({
            'height': cHeight
        }, duration, function() {
            collapse.addClass('open').css('height', 'auto')
        });
    }
});
