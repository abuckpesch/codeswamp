$(function(){
    $('.accordion .accordion-header').on('click', function() {
        let item = $(this).parent();
        let collapse = $(this).siblings('.collapse');
        collapse.clearQueue();
        if (item.hasClass('show')) {
            item.removeClass('show');
            collapse.animate({
                'height': 0
            }, {
                duration: 250,
                complete: function() {
                    collapse.css({height: ''})
                }
            });
        } else {
            let colH = collapse.css('height', 'auto').outerHeight();
            item.addClass('show');
            collapse.css('height', '');
            collapse.animate({
                'height': colH
            }, {
                duration: 250,
                complete: function() {
                    collapse.height('auto')
                }
            });
        }
    });
});
